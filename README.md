> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Alexander Wilson

### LIS4381 Requirements:

*Course Work Links:* 

##### 1. [A1 README.md](a1/README.md "My A1 README.md file")
- Install AMPPS 
- Install JDK
- Install Android Studio and create My First App
- Provide screenshots of installations
- Create Bitbucket repo
- Complete Bitbucket tutorials (bitbucketstationlocation and myteamquotes)
- provide git command descriptions

##### 2. [A2 README.md](a2/README.md "My A2 README.md file")
- Create Healthy Recipes Android App
- Provides screenshots of completed app

##### 3. [A3 README.md](a3/README.md "My A3 README.md file")
- Create ERD based upond business rules
- Provide screenshot of completed ERD
- Provide DB resource links

##### 4. [A4 README.md](a4/README.md "My A4 README.md file")
- Create Webpage Pet Form
- Provide Screenshots of Homepage, working and not working form
- Provide Screenshots of Skill Sets

##### 5. [A5 README.md](a5/README.md "My A5 README.md file")
- Create Page for Petstore Data
- Create Simple Calculator (SS14)
- Create Write/Read (SS15)
- Display All screenshots

##### 6. [P1 README.md](p1/README.md "My P1 README.md file")
 - Create My Business Card App
 - Provide Screenshots of completed Skill Sets (7-9)

##### 7. [P2 README.md](p2/README.md "My P2 README.md file")
- create rss
- create update/delete for petstore

