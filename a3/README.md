> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development
## Alexander Wilson

### Assignment 3 Requirements:

*CheckList:* 

1. Ticket Calculator Mobile App
2. Screenshots of Skill Sets & ERD
3. Links to MYSQL mwb and sql


#### README.md file should include the following items:

* Course-title, name, and Assignment Requirements
* Screenshot of running Android Studio first and second user interface
* Screenshot of all 3 Java Skill Sets running
* Screenshot of ERD
* MYSQL Links



#### Assignment Screenshots:

|    Screenshot of first user interface:             |    Screenshot of second user interface:            |
|----------------------------------------------------|----------------------------------------------------|
|![First User Interface](img/app1.png)               |  ![Second User Interface](img/app2.png)     |



|SS4                             | SS5                             | 
|--------------------------------|---------------------------------|
|![Skill Set 1](img/work.png)    | ![Skill Set 2](img/array.png)   | 

|SS6                             |  ERD                            |
|--------------------------------|---------------------------------|
|![Skill Set 3](img/ss6.png)     |![ERD](img/erd.png)              |           


#### MySQL Links:

*SQL Link:*
[A3 SQL](docs/a3.sql "A3 SQL")

*Workbench Link:*
[A3 MWB](docs/a3.mwb "A3 Workbench")