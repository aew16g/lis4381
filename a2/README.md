> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development
## Alexander Wilson

### Assignment 2 Requirements:

Three Parts:

1. Healthy Recipes Mobile App
2. Chapter Questions
3. Bitbucket repo link

#### README.md file should include the following items:

* Screenshot of running Android Studio first and second user interface
* Screenshot of all 3 Java Skill Sets running
* Bitbucket repo link for this assignment
* Bitbucket repo links


#### Assignment Screenshots:

|    Screenshot of first user interface:             |    Screenshot of second user interface:            |
|----------------------------------------------------|----------------------------------------------------|
|![First User Interface](img/home.png)               |  ![Second User Interface](img/ingredients.png)     |



|EvenOrOdd.java                  | LargestNumber.java              | ArrayAndLoops.java            |
|--------------------------------|---------------------------------|-------------------------------|
|![Skill Set 1](img/even.png)    | ![Skill Set 2](img/largest.png) | ![Skill Set 3](img/array.png) |

