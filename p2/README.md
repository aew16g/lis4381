> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development
## Alexander Wilson

### Project 2 Requirements:

*CheckList:* 

1. Screenshots of Petstore MySQL form and update/delete screenshots
2. Screenshot of home screen 
3. Screenshot of rss



#### README.md file should include the following items:

* Course-title, name, and Project Requirements
* Screenshots of Petstore Data
* Screenshots of portfolio
* Link to local lis4381 web app: http://localhost/repos/lis4381/




#### Assignment Screenshots:

|         petstore:                        |     Screenshot of Petstore Error:            |
|------------------------------------------|----------------------------------------------|
|![First User Interface](img/petstore.png) |  ![Second User Interface](img/error.png)     |

| homepage                                 |                 update                       |
|------------------------------------------|----------------------------------------------|
| ![Third     ](img/homepage.png)          | ![Skill Set 14](img/update.png)              |


| rss                                                                                     |
|-----------------------------------------------------------------------------------------|
| ![Skill Set 2](img/new.png)                                                             |



