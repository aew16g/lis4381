> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development
## Alexander Wilson

### Project 1 Requirements:

*CheckList:* 

1. My Business Card App
2. Screenshots of Skill Sets



#### README.md file should include the following items:

* Course-title, name, and Assignment Requirements
* Screenshot of running Android Studio first and second user interface
* Screenshot of all 3 Java Skill Sets running





#### Assignment Screenshots:

|    Screenshot of first user interface:             |    Screenshot of second user interface:            |
|----------------------------------------------------|----------------------------------------------------|
|![First User Interface](img/first.png)               |  ![Second User Interface](img/second.png)     |



|SS7                               | SS8                             |
|----------------------------------|---------------------------------|
|![Skill Set 1](img/validation.png)| ![Skill Set 2](img/largest.png) |

|SS6                               |
|----------------------------------|
|![Skill Set 3](img/ss9.png)       |          
