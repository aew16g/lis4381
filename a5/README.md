> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development
## Alexander Wilson

### Assignment 5 Requirements:

*CheckList:* 

1. Screenshots of Petstore MySQL form
2. Screenshots of Working Calculator with errors
3. Screenshots of Read/Write (SS15)


#### README.md file should include the following items:

* Course-title, name, and Assignment Requirements
* Screenshots of Petstore Data
* Screenshot of Skill Sets 13,14,15
* Link to local lis4381 web app: http://localhost/repos/lis4381/




#### Assignment Screenshots:

|    Screenshot of Petstore MySQL:         |    Screenshot of Petstore Error:             |
|------------------------------------------|----------------------------------------------|
|![First User Interface](img/database.png) |  ![Second User Interface](img/error.png)     |

| Screenshot of Simple Calculator          |                 4 + 4 = 8                    |
|------------------------------------------|----------------------------------------------|
| ![Third     ](img/simplecalculator.png)  | ![Skill Set 14](img/ss14.png)                |


| Division                                 | Division by 0                                |
|------------------------------------------|----------------------------------------------|
| ![Skill Set 2](img/divide.png)           | ![Skill Set 3](img/divideby.png)             |


|                            Skill Set 13                                                 |
|-----------------------------------------------------------------------------------------|
| ![Skill Set 13](img/ss13.png)                                                           |

|        Skill Set 15                     |  Gettysberg Address                           |
|-----------------------------------------|-----------------------------------------------|
| ![Skill Set 13](img/ss15.png)           |  ![Skillset 15](img/getty.png)                |

