> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development
## Alexander Wilson

### Assignment 4 Requirements:

*CheckList:* 

1. Screenshots of successful/unsucessful Petstore form
2. Screenshots of Homepage
3. Screenshots of Skill Sets


#### README.md file should include the following items:

* Course-title, name, and Assignment Requirements
* Screenshots of Online Portfolio
* Screenshot of all 3 Java Skill Sets running
* Link to local lis4381 web app: http://localhost/repos/lis4381/




#### Assignment Screenshots:

|    Screenshot of Homepage:               |    Screenshot of working Pet Form:           |
|------------------------------------------|----------------------------------------------|
|![First User Interface](img/homepage.png) |  ![Second User Interface](img/sucessful.png) |

| Screenshot of Not-working Pet Form:      |                    SS10                      |
|------------------------------------------|----------------------------------------------|
| ![Third User Interface](img/failed.png)  | ![Skill Set 1](img/first.png)                |


| SS11                                     |SS12                                          |
|------------------------------------------|----------------------------------------------|
| ![Skill Set 2](img/second.png)           | ![Skill Set 3](img/third.png)                |  

      

